package services

import (
	"adbuilder-server/conversion"
	"encoding/json"
	"fmt"
	"net/http"
	"path"

	"adbuilder-server/errors"
	"adbuilder-server/models"

	"github.com/gin-gonic/gin"
	"github.com/iancoleman/strcase"
	"github.com/peterbourgon/diskv"
)

func writeProject(name string, project models.Project, c *gin.Context, db *diskv.Diskv) error {
	encoded, errJSON := json.Marshal(project)
	if errors.IsError(errJSON) {
		return errJSON
	}
	filepath := path.Join(name, c.Param("user_id"), *project.Name+".json")
	return db.Write(filepath, encoded)
}

func extractProject(objects []interface{}, index int) (models.Project, error) {
	var project models.Project
	encoded, errJSON := json.Marshal(objects[index])
	if errors.IsError(errJSON) {
		return project, errJSON
	}
	errJSON2 := json.Unmarshal(encoded, &project)
	if errors.IsError(errJSON2) {
		return project, errJSON2
	}
	return project, nil
}

// UpdateProject a Project information on the disk
// if name changes it delete the old file and create a new one
func UpdateProject(done chan error, name string, model interface{}, c *gin.Context, db *diskv.Diskv, search func(name string, c *gin.Context) ([]interface{}, error)) {
	var body map[string]interface{}
	err := c.BindJSON(&body)
	if err == nil {
		values := c.Request.URL.Query()
		exQuery := c.Request.URL.Query()
		// create the query to find the project
		for key, value := range body {
			if value != nil && key != "name" {
				val, errBytes := conversion.GetJSON(value)
				if errors.IsError(errBytes) {
					done <- errBytes
					return
				}
				// fmt.Println("query build:", strcase.ToLowerCamel(key), val)
				switch val.(type) {
				case []uint8:
					values.Add(strcase.ToLowerCamel(key), string(val.([]byte)))
				default:
					values.Add(strcase.ToLowerCamel(key), string(val.(string)))
				}
			}
		}
		c.Request.URL.RawQuery = values.Encode()
		// fmt.Println("PUT 1st query:", c.Request.URL.Query(), reflect.DeepEqual(c.Request.URL.Query(), values))
		objects, errSearch := search(name, c)
		if errors.IsError(errSearch) {
			done <- nil
			return
		}
		if len(objects) == 0 {
			// if object is not found
			// it means that it need modifications
			// so search it by name
			c.Request.URL.RawQuery = exQuery.Encode()
			// fmt.Println("PUT 2nd query:", c.Request.URL.Query(), exQuery)
			objects2, errSearch2 := search(name, c)
			if errors.IsError(errSearch2) {
				done <- nil
				return
			}
			if len(objects2) > 0 {
				project, errJSON := extractProject(objects2, 0)
				if errors.IsError(errJSON) {
					c.JSON(http.StatusBadRequest, gin.H{
						"status":  http.StatusBadRequest,
						"message": fmt.Sprintf("Error in project json encoding: %q", errJSON),
					})
					done <- nil
					return
				}
				created := false
				// replace value in existing project
				// fmt.Println(body.Name != nil, *body.Name, *project.Name, *body.Name != *project.Name)
				if body["name"] != nil && body["name"].(string) != "" && body["name"].(string) != *project.Name {
					// delete old file if name has changed
					err = db.Erase(path.Join(name, c.Param("user_id"), *project.Name+".json"))
					if errors.IsError(err) {
						done <- nil
						return
					}
					val := body["name"].(string)
					project.Name = &val
					created = true
				}
				if body["options"] != nil && len(body["options"].([]interface{})) > 0 {
					var options []string
					val := body["options"].([]interface{})
					for _, value := range val {
						options = append(options, value.(string))
					}
					project.Options = options
				}
				if body["elements"] != nil && len(body["elements"].([]interface{})) > 0 {
					val := body["elements"].([]interface{})
					project.Elements = val
				}
				compEmtpyTemplate := (*models.Template)(nil)
				if body["template"] != nil && body["template"] != compEmtpyTemplate {
					t := body["template"].(map[string]interface{})
					val := models.Template{
						Name: t["name"].(string),
						URL:  t["url"].(string),
					}
					project.Template = &val
				}
				if body["maxTime"].(float64) != 0 && body["maxTime"] != 0 {
					val := body["maxTime"].(float64)
					project.MaxTime = &val
				}
				if body["css"].(string) != "" {
					val := body["css"].(string)
					project.CSS = &val
				}

				errWrite := writeProject(name, project, c, db)
				if errors.IsError(errWrite) {
					c.JSON(http.StatusBadRequest, gin.H{
						"status":  http.StatusBadRequest,
						"message": fmt.Sprintf("Error in project json encoding: %q", errWrite),
					})
					done <- nil
					return
				}
				if created {
					c.JSON(http.StatusCreated, project)
				} else {
					c.JSON(http.StatusOK, project)
				}
				done <- nil
				return
			}
			c.JSON(http.StatusBadRequest, gin.H{
				"status":  http.StatusBadRequest,
				"message": fmt.Sprintf("Project %q doesn't exist", c.Param("project_name")),
			})
			done <- nil
			return
		} else if len(objects) == 1 {
			project, errJSON := extractProject(objects, 0)
			if errors.IsError(errJSON) {
				c.JSON(http.StatusBadRequest, gin.H{
					"status":  http.StatusBadRequest,
					"message": fmt.Sprintf("Error in project json encoding: %q", errJSON),
				})
				done <- nil
				return
			}
			// if object is found
			// it means there is juste the name that change
			// replace name if it is not the same
			if body["name"] != nil && body["name"] != *project.Name {
				// delete old file if name has changed
				err = db.Erase(path.Join(name, c.Param("user_id"), *project.Name+".json"))
				if errors.IsError(err) {
					done <- nil
					return
				}
				val := body["name"].(string)
				project.Name = &val

				errWrite := writeProject(name, project, c, db)
				if errors.IsError(errWrite) {
					c.JSON(http.StatusBadRequest, gin.H{
						"status":  http.StatusBadRequest,
						"message": fmt.Sprintf("Error in project json encoding: %q", errWrite),
					})
					done <- nil
					return
				}
				c.JSON(http.StatusCreated, project)
			} else {
				c.JSON(http.StatusNotModified, gin.H{
					"status":  http.StatusNotModified,
					"message": fmt.Sprintf("Project %q is the same", *project.Name),
				})
			}
			done <- nil
			return
		}
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  http.StatusBadRequest,
			"message": fmt.Sprintf("Project %q doesn't exist", c.Param("project_name")),
		})
		done <- nil
		return
	}
	done <- err
	return
}
