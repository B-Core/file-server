package stringutils

import (
	"fmt"
)

// ConvertToStringSlice converts a slice of uint8
// to a slice of string
func ConvertToStringSlice(src []interface{}) []string {
	data := make([]string, len(src))
	for i, v := range data {
		data[i] = fmt.Sprintf(v)
	}
	return data
}

// ConvertToFloat64Slice converts a slice of uint8
// to a slice of Float64
func ConvertToFloat64Slice(src []string) []float64 {
	data := make([]float64, len(src))
	for i, v := range data {
		data[i] = float64(v)
	}
	return data
}
