package stringutils

import (
	"testing"
)

type FormatToStructFieldNameTest struct {
	input  string
	output string
}

var tests = []FormatToStructFieldNameTest{
	{"a", "a"},
	{"snake", "Snake"},
	{"A", "A"},
	{"ID", "ID"},
	{"MOTD", "Motd"},
	{"snake", "Snake"},
	{"snake_test", "Snake_test"},
	{"snakeID", "Snakeid"},
	{"snakeIDGoogle", "Snakeidgoogle"},
	{"linuxMOTD", "Linuxmotd"},
	{"OMGWTFBBQ", "Omgwtfbbq"},
	{"omg_wtf_bbq", "Omg_wtf_bbq"},
}

func TestFormatToStructFieldName(t *testing.T) {
	for _, test := range tests {
		if FormatToStructFieldName(test.input) != test.output {
			t.Errorf(`FormatToStructFieldName("%s"), wanted "%s", got \%s"`, test.input, test.output, FormatToStructFieldName(test.input))
		}
	}
}
