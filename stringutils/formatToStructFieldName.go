package stringutils

import (
	"regexp"
	"strings"
	"unicode"
)

var regID = regexp.MustCompile("^(?i)id$")
var regUUID = regexp.MustCompile("^(?i)uuid$")
var regCSS = regexp.MustCompile("^(?i)css$")

// FormatToStructFieldName format a string
// to have the go struct Field Name convention
func FormatToStructFieldName(in string) string {
	runes := []rune(in)
	length := len(runes)

	var out []rune
	for i := 0; i < length; i++ {
		if i == 0 && unicode.IsLower(runes[i]) && len(runes) > 1 {
			out = append(out, unicode.ToUpper(runes[i]))
		} else if i > 0 && unicode.IsUpper(runes[i]) {
			out = append(out, unicode.ToLower(runes[i]))
		} else {
			out = append(out, runes[i])
		}
	}

	sOut := string(out)

	if regID.MatchString(sOut) || regUUID.MatchString(sOut) || regCSS.MatchString(sOut) {
		sOut = strings.ToUpper(sOut)
	}
	return sOut
}
