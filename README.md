# adbuilder-server

Simple adbuilder-server written in GO

## Installation

+ Install Golang > 1.5
+ Install [govendor](https://github.com/kardianos/govendor) `go get -u github.com/kardianos/govendor`
+ Get dependencies (govendor sync)

## Build

```
go build -tags=jsoniter .
```