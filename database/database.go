package database

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"path"
	"reflect"
	"runtime"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/json-iterator/go"
	"github.com/peterbourgon/diskv"

	"adbuilder-server/config"
	"adbuilder-server/conversion"
	"adbuilder-server/errors"
	"adbuilder-server/models"
	"adbuilder-server/services"
)

var json = jsoniter.ConfigCompatibleWithStandardLibrary

// Simplest transform function: put all the data files into the base dir.
var flatTransform = func(s string) []string { return []string{} }
var _, dirname, _, _ = runtime.Caller(0)
var dbFileName = path.Join(dirname, "..", conf.DB.Path)

// Initialize a new diskv store, rooted at "my-data-dir"
var db = diskv.New(diskv.Options{
	BasePath:     dbFileName,
	Transform:    flatTransform,
	CacheSizeMax: (1024 * 1024) * 100, // 100MB cache
})

var conf = config.Conf()

func fileExists(path string) bool {
	// detect if file exists
	_, err := os.Stat(path)
	if os.IsNotExist(err) {
		return false
	}
	return true
}

func existsIn(array []interface{}, object interface{}) bool {
	found := false
	for _, ob := range array {
		if ob.(map[string]interface{})["uuid"].(string) == object.(map[string]interface{})["uuid"].(string) {
			found = true
			break
		}
	}
	return found
}

func typeof(v interface{}) string {
	return reflect.ValueOf(v).String()
}

func getInterfaceInString(i interface{}) string {
	ret := ""
	switch i.(type) {
	case []uint8:
		ret = string(i.([]byte))
	default:
		ret = string(i.(string))
	}
	return strings.TrimSpace(ret)
}

func execQuerySearch(object interface{}, query url.Values, c *gin.Context) (bool, error) {
	found := !(len(query) > 0)
	objs := object.(map[string]interface{})
	for keyName, key := range query {
		for _, value := range key {
			// check type and convert if needed
			// if it is a map, do a for loop and check the values
			// they must be all equal
			// else we compare the strings
			srcVal, err := conversion.GetJSON(objs[keyName])
			if errors.IsError(err) {
				return false, err
			}
			inVal, err2 := conversion.GetJSON(value)
			if errors.IsError(err2) {
				return false, err2
			}
			// fmt.Println(keyName, getInterfaceInString(inVal))
			// fmt.Println(keyName, getInterfaceInString(srcVal))
			// fmt.Println(getInterfaceInString(srcVal) == getInterfaceInString(inVal))
			found = getInterfaceInString(srcVal) == getInterfaceInString(inVal)
			if !found {
				break
			}
		}
		if !found {
			break
		}
	}
	return found, nil
}

func search(name string, c *gin.Context) ([]interface{}, error) {
	filepath := path.Join(dirname, "..", conf.DB.Path, name, c.Param("user_id"))
	files, err := ioutil.ReadDir(filepath)
	var objects []interface{}
	if err != nil {
		return objects, err
	}
	for _, f := range files {
		var object interface{}
		value, errRead := db.Read(path.Join(name, c.Param("user_id"), f.Name()))
		if errors.IsError(errRead) {
			c.JSON(http.StatusInternalServerError, gin.H{
				"status":  http.StatusInternalServerError,
				"message": fmt.Sprintf("Error database reading: %q", errRead),
			})
			return objects, err
		}
		errJSON := json.Unmarshal(value, &object)
		if errors.IsError(errJSON) {
			c.JSON(http.StatusBadRequest, gin.H{
				"status":  http.StatusBadRequest,
				"message": fmt.Sprintf("Error in json decoding: %q", errJSON),
			})
			return objects, errJSON
		}
		query := c.Request.URL.Query()
		// fmt.Println("search query:", query)
		found, errSearch := execQuerySearch(object, query, c)
		if found && !existsIn(objects, object) {
			objects = append(objects, object)
		}
		if errSearch != nil {
			return objects, errSearch
		}
	}
	return objects, nil
}

func createFile(done chan bool, path string) {
	// create file if not exists
	if !fileExists(path) {
		var file, err = os.Create(path)
		if errors.IsError(err) {
			return
		}
		defer file.Close()
		fmt.Println("==> done creating file", path)
	}
	done <- true
}

func createDir(done chan bool, path string) {
	if !fileExists(path) {
		err := os.MkdirAll(path, os.ModePerm)
		if errors.IsError(err) {
			return
		}
		fmt.Println("==> done creating dirs", path)
	}
	done <- true
}

func readDB(name string, c *gin.Context) error {
	// switch name {
	// case "projects":
	// 	var projects []interface{}
	// 	filepath := path.Join(dirname, "..", conf.DB.Path, name, c.Param("user_id"))
	// 	d := make(chan bool)
	// 	go createDir(d, filepath)
	// 	<-d
	// 	objects, errSearch := search(name, c)
	// 	if errors.IsError(errSearch) { return errSearch }

	// 	for _, p := range objects {
	// 		projects = append(projects, p)
	// 	}

	// 	c.JSON(http.StatusOK, projects)
	// }
	filepath := path.Join(dirname, "..", conf.DB.Path, name, c.Param("user_id"))
	done := make(chan bool)
	go createDir(done, filepath)
	<-done
	objects, errSearch := search(name, c)
	if errors.IsError(errSearch) {
		return errSearch
	}
	c.JSON(http.StatusOK, objects)
	return nil
}

// Get retrieve the content of the named database
// based on the query
// if no query is present, all is retrieved
func Get(name string, c *gin.Context) error {
	return readDB(name, c)
}

// Post creates a new entry in the database
// if id doesn't exists yet
func Post(name string, model interface{}, c *gin.Context) error {
	filepath := path.Join(dirname, "..", conf.DB.Path, name, c.Param("user_id"))
	if !fileExists(filepath) {
		doneDir := make(chan bool)
		go createDir(doneDir, filepath)
		<-doneDir
	}

	switch model.(type) {
	default:
		c.JSON(http.StatusNotFound, gin.H{
			"status":  http.StatusNotFound,
			"message": fmt.Sprintf("not database named %q", name),
		})
		return nil
	case models.Project:
		var body models.ProjectBody
		err := c.BindJSON(&body)
		if err == nil {
			var project *models.Project
			project, err = models.CreateProject(body)
			if err != nil {
				c.JSON(http.StatusBadRequest, gin.H{
					"status":  http.StatusBadRequest,
					"message": fmt.Sprintf("Error in project creation: %q", err),
				})
				return nil
			}

			filepath = path.Join(dirname, "..", conf.DB.Path, name, c.Param("user_id"), *project.Name+".json")

			if !fileExists(filepath) {
				done := make(chan bool)
				go createFile(done, filepath)
				<-done

				encoded, errJSON := json.Marshal(project)
				if errJSON != nil {
					c.JSON(http.StatusBadRequest, gin.H{
						"status":  http.StatusBadRequest,
						"message": fmt.Sprintf("Error in project json decoding: %q", errJSON),
					})
					return nil
				}
				db.Write(path.Join(name, c.Param("user_id"), *project.Name+".json"), encoded)
				c.JSON(http.StatusCreated, project)
				return nil
			}
			c.JSON(http.StatusForbidden, gin.H{
				"status":  http.StatusForbidden,
				"message": fmt.Sprintf("Project %q already exists", *project.Name),
			})
			return nil
		}
	}
	c.JSON(http.StatusNotFound, gin.H{
		"status":  http.StatusNotFound,
		"message": fmt.Sprintf("no database with model %T", model),
	})
	return nil
}

// Delete gets an object in the db
// and delete it if it exists
func Delete(name string, model interface{}, c *gin.Context) error {
	switch model.(type) {
	case models.Project:
		filename := c.Param("project_name") + ".json"
		filepath := path.Join(name, c.Param("user_id"), filename)
		err := db.Erase(filepath)
		if errors.IsError(err) {
			c.JSON(http.StatusBadRequest, gin.H{
				"status":  http.StatusBadRequest,
				"message": fmt.Sprintf("Project %q doesn't exist", c.Param("project_name")),
			})
			return nil
		}
		c.JSON(http.StatusOK, "Project "+c.Param("project_name")+" deleted")
		return nil
	}
	c.JSON(http.StatusNotFound, gin.H{
		"status":  http.StatusNotFound,
		"message": fmt.Sprintf("no database named %q", name),
	})
	return nil
}

// Update gets an object in the db
// and updates it if it exists
func Update(name string, model interface{}, c *gin.Context) error {
	done := make(chan error)
	switch model.(type) {
	case models.Project:
		go services.UpdateProject(done, name, model, c, db, search)
	}
	err := <-done
	if err != nil {
		c.JSON(http.StatusNotFound, err)
	}
	return nil
}
