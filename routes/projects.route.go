package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/json-iterator/go"

	"adbuilder-server/config"
	"adbuilder-server/database"
	"adbuilder-server/models"
)

var json = jsoniter.ConfigCompatibleWithStandardLibrary
var conf = config.Conf()
var routes = conf.Routes.Projects

func getProjects(c *gin.Context) {
	query := c.Request.URL.Query()
	if c.Param("project_name") != "" {
		query["name"] = []string{
			c.Param("project_name"),
		}
	}
	c.Request.URL.RawQuery = query.Encode()
	err := database.Get("projects", c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
	}
}

func createProject(c *gin.Context) {
	var project models.Project
	err := database.Post("projects", project, c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
	}
}

func deleteProject(c *gin.Context) {
	var project models.Project
	err := database.Delete("projects", project, c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
	}
}

func updateProject(c *gin.Context) {
	var project models.Project
	query := c.Request.URL.Query()
	if c.Param("project_name") != "" {
		query["name"] = []string{
			c.Param("project_name"),
		}
	}
	c.Request.URL.RawQuery = query.Encode()
	err := database.Update("projects", project, c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
	}
}

// AttachRoute attach the defined routes to the given router
func AttachRoute(routeGroup string, router *gin.Engine) {
	switch routeGroup {
	case "projects":
		{
			group := router.Group(routes["root"])
			{
				group.GET(routes["projects"], getProjects)
				group.POST(routes["projects"], createProject)
				group.GET(routes["project"], getProjects)
				group.DELETE(routes["project"], deleteProject)
				group.PUT(routes["project"], updateProject)
			}
		}
	}
}
