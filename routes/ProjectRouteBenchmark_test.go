package routes

import (
	"adbuilder-server/config"
	"adbuilder-server/server"
	"adbuilder-server/testutils"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strconv"
	"strings"
	"testing"
)

var result int

func BenchmarkPOSTProject(b *testing.B) {
	conf := config.Conf()
	router := server.CreateServer()
	AttachRoute("projects", router)
	// create the base project
	route := testutils.GenerateParamsRoutes(conf.Routes.Projects["root"]+conf.Routes.Projects["projects"], "test-perf")
	recorder := httptest.NewRecorder()

	var done int

	maxWanted := 2000
	// perfs := make([]int, 16)

	for i := 0; i < b.N; i++ {
		data := `{"name":"test-perf-` + strconv.Itoa(i) + `","options":[],"elements":[{"name":"test-perf-` + strconv.Itoa(i) + `","options":[],"elements":[{"name":"test-perf-` + strconv.Itoa(i) + `","options":[],"elements":[{"name":"test-perf-` + strconv.Itoa(i) + `","options":[],"elements":[],"maxTime":10.00,"css":""}],"maxTime":10.00,"css":""}],"maxTime":10.00,"css":""}],"maxTime":10.00,"css":""}`
		req, _ := http.NewRequest("POST", route, strings.NewReader(string(data)))
		router.ServeHTTP(recorder, req)
		done = i
	}

	result = done

	if result >= (maxWanted - 1) {
		fmt.Println("--------------------- SUCCESS -------------------------")
		fmt.Println(" ")
		fmt.Println(strconv.Itoa(result+1) + " project were POSTed, goal: " + strconv.Itoa(maxWanted))
		fmt.Println(" ")
		fmt.Println("---------------------- END RESULTS ------------------------")
	} else {
		fmt.Println("--------------------- FAILED -------------------------")
		fmt.Println(" ")
		fmt.Println(strconv.Itoa(result+1) + " project were POSTed, goal: " + strconv.Itoa(maxWanted))
		fmt.Println(" ")
		fmt.Println("---------------------- END RESULTS ------------------------")
	}
}
