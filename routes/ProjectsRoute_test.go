package routes

import (
	"net/http"
	"net/http/httptest"
	"path"
	"runtime"
	"strings"
	"testing"

	"adbuilder-server/config"
	"adbuilder-server/models"
	"adbuilder-server/server"
	"adbuilder-server/testutils"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
)

// Define the suite, and absorb the built-in basic suite
// functionality from testify - including a T() method which
// returns the current testing context
type ProjectRouteTestSuite struct {
	suite.Suite
	Router   *gin.Engine
	Route    string
	Project1 models.ProjectBody
}

func (suite *ProjectRouteTestSuite) SetupTest() {
	conf = config.Conf()
	router := server.CreateServer()
	AttachRoute("projects", router)

	suite.Router = router

	var options []string
	var css string
	var elements []interface{}
	var template models.Template
	name := "test"
	maxTime := 10.00

	suite.Project1 = models.ProjectBody{
		Name:     &name,
		Options:  &options,
		Elements: &elements,
		Template: &template,
		MaxTime:  &maxTime,
		CSS:      &css,
	}
}

func (suite *ProjectRouteTestSuite) TestGetNoProject() {
	suite.Route = testutils.GenerateParamsRoutes(conf.Routes.Projects["root"]+conf.Routes.Projects["projects"], "test")
	recorder := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", suite.Route, nil)
	suite.Router.ServeHTTP(recorder, req)

	assert.Equal(suite.T(), 200, recorder.Code)
	assert.Equal(suite.T(), "null", recorder.Body.String())
}

func (suite *ProjectRouteTestSuite) TestPostProject() {
	suite.Route = testutils.GenerateParamsRoutes(conf.Routes.Projects["root"]+conf.Routes.Projects["projects"], "test2")
	recorder := httptest.NewRecorder()
	data, _ := json.Marshal(suite.Project1)
	req, _ := http.NewRequest("POST", suite.Route, strings.NewReader(string(data)))
	suite.Router.ServeHTTP(recorder, req)

	assert.Equal(suite.T(), 201, recorder.Code)
	var project models.Project
	err := json.Unmarshal(recorder.Body.Bytes(), &project)
	if assert.Nil(suite.T(), err, "error in json decoding") {
		name := project.Name
		options := project.Options
		elements := project.Elements
		template := project.Template
		maxTime := project.MaxTime
		CSS := project.CSS

		assert.Equal(suite.T(), *name, "test")
		assert.Equal(suite.T(), options, []string{})
		assert.Equal(suite.T(), elements, []interface{}{})
		assert.Equal(suite.T(), *template, models.Template{
			Name: "",
			URL:  "",
		})
		assert.Equal(suite.T(), *maxTime, 10.00)
		assert.Equal(suite.T(), *CSS, "")

		_, dirname, _, _ := runtime.Caller(0)
		filepath := path.Join(dirname, "..", "..", "database", conf.DB.Path, conf.Routes.Projects["root"], "test2", "test.json")
		assert.FileExists(suite.T(), filepath)

		// test that the project can't be overriden by another POST
		req2, _ := http.NewRequest("POST", suite.Route, strings.NewReader(string(data)))
		recorder2 := httptest.NewRecorder()
		suite.Router.ServeHTTP(recorder2, req2)
		assert.Equal(suite.T(), 403, recorder2.Code)
	}
}

func (suite *ProjectRouteTestSuite) TestGetProjectWithStringQuery() {
	suite.Route = testutils.GenerateParamsRoutes(conf.Routes.Projects["root"]+conf.Routes.Projects["projects"], "test-query-string")
	var css string
	name := "test-query-string1"
	maxTime := 10.00
	project1 := models.ProjectBody{
		Name:     &name,
		Options:  &[]string{},
		Elements: &[]interface{}{},
		Template: &models.Template{
			Name: "test-template",
			URL:  "http://test-template.com",
		},
		MaxTime: &maxTime,
		CSS:     &css,
	}
	data, _ := json.Marshal(project1)
	// create a new project
	req, _ := http.NewRequest("POST", suite.Route, strings.NewReader(string(data)))
	recorder := httptest.NewRecorder()
	suite.Router.ServeHTTP(recorder, req)
	assert.Equal(suite.T(), 201, recorder.Code)

	var css2 string
	name2 := "test-query-string2"
	maxTime2 := 10.00
	project2 := models.ProjectBody{
		Name:     &name2,
		Options:  &[]string{},
		Elements: &[]interface{}{},
		Template: project1.Template,
		MaxTime:  &maxTime2,
		CSS:      &css2,
	}
	data2, _ := json.Marshal(project2)
	// create a new project
	req2, _ := http.NewRequest("POST", suite.Route, strings.NewReader(string(data2)))
	recorder2 := httptest.NewRecorder()
	suite.Router.ServeHTTP(recorder2, req2)
	assert.Equal(suite.T(), 201, recorder2.Code)

	// test to GET the projects that i just created with a query
	// fetching only the project by name "test-query-string1"
	recorder3 := httptest.NewRecorder()
	req3, _ := http.NewRequest("GET", suite.Route+"?name=test-query-string1", nil)

	suite.Router.ServeHTTP(recorder3, req3)
	assert.Equal(suite.T(), 200, recorder3.Code)
	var projects []models.Project
	err := json.Unmarshal(recorder3.Body.Bytes(), &projects)
	if assert.Nil(suite.T(), err, "error in json decoding") {
		for _, project := range projects {
			assert.Equal(suite.T(), *project.Name, *project1.Name)
			assert.Equal(suite.T(), project.Options, *project1.Options)
			assert.Equal(suite.T(), project.Elements, *project1.Elements)
			assert.Equal(suite.T(), *project.Template, *project1.Template)
			assert.Equal(suite.T(), *project.MaxTime, *project1.MaxTime)
			assert.Equal(suite.T(), *project.CSS, *project1.CSS)
		}
		// fmt.Println(projects)
		assert.Equal(suite.T(), 1, len(projects))
	}

	// test to GET the projects that i just created with a query
	// fetching only the project by name "name=test-query-string2"
	recorder4 := httptest.NewRecorder()
	req4, _ := http.NewRequest("GET", suite.Route+`?name=test-query-string2`, nil)

	suite.Router.ServeHTTP(recorder4, req4)
	assert.Equal(suite.T(), 200, recorder4.Code)
	var projects2 []models.Project
	err = json.Unmarshal(recorder4.Body.Bytes(), &projects2)
	if assert.Nil(suite.T(), err, "error in json decoding") {
		for _, project := range projects2 {
			assert.Equal(suite.T(), project.Name, project2.Name)
			assert.Equal(suite.T(), project.Options, *project2.Options)
			assert.Equal(suite.T(), project.Elements, *project2.Elements)
			assert.Equal(suite.T(), *project.Template, *project2.Template)
			assert.Equal(suite.T(), *project.MaxTime, *project2.MaxTime)
			assert.Equal(suite.T(), *project.CSS, *project2.CSS)
		}
		assert.Equal(suite.T(), 1, len(projects2))
	}

	// test to GET the projects that i just created with a query
	// fetching all the project by template "{"name": "test-template"}"
	recorder5 := httptest.NewRecorder()
	req5, _ := http.NewRequest("GET", suite.Route+`?template={"name": "test-template", "url": "http://test-template.com"}`, nil)

	suite.Router.ServeHTTP(recorder5, req5)
	assert.Equal(suite.T(), 200, recorder5.Code)
	var projects3 []models.Project
	err = json.Unmarshal(recorder5.Body.Bytes(), &projects3)
	if assert.Nil(suite.T(), err, "error in json decoding") {
		for index, project := range projects3 {
			if index == 0 {
				assert.Equal(suite.T(), project.Name, project1.Name)
				assert.Equal(suite.T(), project.Options, *project1.Options)
				assert.Equal(suite.T(), project.Elements, *project1.Elements)
				assert.Equal(suite.T(), *project.Template, *project1.Template)
				assert.Equal(suite.T(), *project.MaxTime, *project1.MaxTime)
				assert.Equal(suite.T(), *project.CSS, *project1.CSS)
			} else if index == 1 {
				assert.Equal(suite.T(), project.Name, project2.Name)
				assert.Equal(suite.T(), project.Options, *project2.Options)
				assert.Equal(suite.T(), project.Elements, *project2.Elements)
				assert.Equal(suite.T(), *project.Template, *project2.Template)
				assert.Equal(suite.T(), *project.MaxTime, *project2.MaxTime)
				assert.Equal(suite.T(), *project.CSS, *project2.CSS)
			}
		}
		assert.Equal(suite.T(), 2, len(projects3))
	}
}

func (suite *ProjectRouteTestSuite) TestDeleteProject() {
	// create the project to DELETE
	var css string
	name := "test-delete"
	maxTime := 10.00
	project := models.ProjectBody{
		Name:     &name,
		Options:  &[]string{},
		Elements: &[]interface{}{},
		Template: &models.Template{
			Name: "",
			URL:  "",
		},
		MaxTime: &maxTime,
		CSS:     &css,
	}
	suite.Route = testutils.GenerateParamsRoutes(conf.Routes.Projects["root"]+conf.Routes.Projects["projects"], "test-delete")
	recorder := httptest.NewRecorder()
	data, _ := json.Marshal(project)
	req, _ := http.NewRequest("POST", suite.Route, strings.NewReader(string(data)))
	suite.Router.ServeHTTP(recorder, req)

	assert.Equal(suite.T(), 201, recorder.Code)
	err := json.Unmarshal(recorder.Body.Bytes(), &project)
	if assert.Nil(suite.T(), err, "error in json decoding") {
		suite.Route = testutils.GenerateParamsRoutes(conf.Routes.Projects["root"]+conf.Routes.Projects["project"], "test-delete")
		// test the DELETE
		req2, _ := http.NewRequest("DELETE", suite.Route, nil)
		recorder2 := httptest.NewRecorder()
		suite.Router.ServeHTTP(recorder2, req2)
		assert.Equal(suite.T(), 200, recorder2.Code)
		assert.Equal(suite.T(), "\"Project test-delete deleted\"", recorder2.Body.String())
	}
}

func (suite *ProjectRouteTestSuite) TestPutProject() {
	// create the base project
	suite.Route = testutils.GenerateParamsRoutes(conf.Routes.Projects["root"]+conf.Routes.Projects["projects"], "test-put")
	recorder := httptest.NewRecorder()

	data := `{"name":"test-put","options":[],"elements":[],"maxTime":10.00,"css":""}`

	req, _ := http.NewRequest("POST", suite.Route, strings.NewReader(string(data)))
	suite.Router.ServeHTTP(recorder, req)

	assert.Equal(suite.T(), 201, recorder.Code)

	// PUT the project
	var project models.Project
	suite.Route = testutils.GenerateParamsRoutes(conf.Routes.Projects["root"]+conf.Routes.Projects["project"], "test-put")
	recorder2 := httptest.NewRecorder()

	data2 := `{"name":"test-put","options":["test", "test2"],"elements":[{"name":"test-put-inside","options":["test", "test2"],"elements":[{"name":"test-put-inside2","options":["test", "test2"],"elements":[],"maxTime":5.57,"css":"#test { display: block }"}],"maxTime":5.57,"css":"#test { display: block }"}],"template":{"name": "test-template", "url": "http://test-template"},"maxTime":5.57,"css":"#test { display: block }"}`

	req2, _ := http.NewRequest("PUT", suite.Route, strings.NewReader(string(data2)))
	suite.Router.ServeHTTP(recorder2, req2)

	assert.Equal(suite.T(), 200, recorder2.Code)
	err := json.Unmarshal(recorder2.Body.Bytes(), &project)
	if assert.Nil(suite.T(), err, "error in json decoding") {
		assert.Equal(suite.T(), "test-put", *project.Name)
		assert.Equal(suite.T(), []string{"test", "test2"}, project.Options)
		assert.Equal(suite.T(), 1, len(project.Elements))
		assert.Equal(suite.T(), models.Template{
			Name: "test-template",
			URL:  "http://test-template",
		}, *project.Template)
		assert.Equal(suite.T(), 5.57, *project.MaxTime)
		assert.Equal(suite.T(), `#test { display: block }`, *project.CSS)
	}
}

func TestProjectsRoute(t *testing.T) {
	suite.Run(t, new(ProjectRouteTestSuite))
}
