package config

import (
	"os"
	"log"
	"time"
	"path"
	"path/filepath"
	"runtime"
	"strings"
	"github.com/tkanos/gonfig"
)

// DB structure for the project
type DB struct {
	Path string						`json:"path, string"`
	Timeout time.Duration `json:"timeout, int"`
}

// Routes structure for the project
type Routes struct {
	Projects map[string]string		`json:"projects"`
	Components map[string]string	`json:"components"`
	Galerie map[string]string			`json:"galerie"`
	Templates map[string]string		`json:"templates"`
	Infos map[string]string				`json:"infos"`
	Upload map[string]string			`json:"upload"`
	Download map[string]string		`json:"download"`
	Public map[string]string			`json:"public"`
	Users map[string]string				`json:"users"`
}

// Configuration structure
// defines the variables needed by the project
type Configuration struct {
	Address string						`json:"address, string"`
	Port string								`json:"port, string"`
	Routes Routes 						`json:"routes"`
	DB DB											`json:"DB"`
	ConnectionString string
}

func getFileName() string {
	env := os.Getenv("ENV")
	if len(env) == 0 {
		env = "development"
	}
	filename := []string{"config.", env, ".json"}
	_, dirname, _, _ := runtime.Caller(0)
	file, err := filepath.Abs(path.Join(filepath.Dir(dirname), strings.Join(filename, "")));
	if err != nil { log.Fatal(err) }
	filePath := file

	return filePath
}

// Conf return the configuration file
func Conf() Configuration {
	env := os.Getenv("Connection_String")
	configuration := Configuration{}
	err := gonfig.GetConf(getFileName(), &configuration)
	configuration.ConnectionString = env;
	if err != nil { log.Fatal(err) }
	return configuration
}