package testutils

import "regexp"

var paramsRegExp = regexp.MustCompile(`(:\w+)+`)

// GenerateParamsRoutes returns a []bytes string with all route parameters replaced by replacement
func GenerateParamsRoutes(route string, replacement string) string {
	return paramsRegExp.ReplaceAllString(route, replacement)
}
