#! bin/bash
MAX_TIME="${2:-5}s"

rm -rf database/testDB/
ENV="test" go test adbuilder-server/routes -bench=. -benchtime=${MAX_TIME}