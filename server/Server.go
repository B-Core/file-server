package server

import (
	"github.com/gin-gonic/gin"
	"adbuilder-server/config"
)

var conf = config.Conf();

// StartServer attaches all routes to the server instance
// and start listening
func StartServer(router *gin.Engine) *gin.Engine {
	router.Run(conf.Address + ":" + conf.Port);
	return router;
}

// CreateServer creates a Gin server and returns it
func CreateServer() *gin.Engine {
	server := gin.Default()
	return server
}