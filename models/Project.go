package models

import (
	"reflect"

	"github.com/satori/go.uuid"
)

// Template represents Template object structure
type Template struct {
	Name string `json:"name, string"`
	URL  string `json:"url, string"`
}

// ProjectBody represents the arguments needed
// to create a project
type ProjectBody struct {
	Name     *string        `json:"name"`
	Options  *[]string      `json:"options"`
	Elements *[]interface{} `json:"elements"`
	Template *Template      `json:"template"`
	MaxTime  *float64       `json:"maxTime"`
	CSS      *string        `json:"css"`
}

// Project represents the main Project object
// stored in the database
type Project struct {
	UUID     uuid.UUID     `json:"uuid, string"`
	Name     *string       `json:"name"`
	Options  []string      `json:"options"`
	Elements []interface{} `json:"elements"`
	Template *Template     `json:"template"`
	MaxTime  *float64      `json:"maxTime, float64"`
	CSS      *string       `json:"css"`
}

// Map returns a map[string]interface{}
// of a ProjectBody
func (p *ProjectBody) Map() map[string]interface{} {
	v := reflect.ValueOf(p).Elem()
	values := make(map[string]interface{})

	for i := 0; i < v.NumField(); i++ {
		values[v.Type().Field(i).Name] = v.Field(i).Interface()
	}

	return values
}

// Map returns a map[string]interface{}
// of a Project
func (p *Project) Map() map[string]interface{} {
	v := reflect.ValueOf(p).Elem()
	values := make(map[string]interface{})

	for i := 0; i < v.NumField(); i++ {
		values[v.Type().Field(i).Name] = v.Field(i).Interface()
	}

	return values
}

// CreateProject return a fully fledged Project
func CreateProject(project ProjectBody) (*Project, error) {
	var p Project
	id, err := uuid.NewV4()
	if err != nil {
		return &p, err
	}

	var name, CSS *string
	var template *Template
	maxTime := 10.00
	options := []string{}
	elements := []interface{}{}

	if project.Name != nil {
		name = project.Name
	}
	if project.Options != nil {
		options = *project.Options
	}
	if project.Elements != nil {
		elements = *project.Elements
	}
	if project.Template != nil {
		template = project.Template
	}
	if project.MaxTime != nil {
		maxTime = *project.MaxTime
	}
	if project.CSS != nil {
		CSS = project.CSS
	}

	p = Project{
		id,
		name,
		options,
		elements,
		template,
		&maxTime,
		CSS,
	}
	return &p, nil
}
