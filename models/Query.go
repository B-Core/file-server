package models

// Query structure to represents the possible searches on a db field
type Query struct {
	ID   string `json:"id, string, omitempty"`
	Name string `json:"name, string, omitempty"`
}
