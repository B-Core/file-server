package conversion

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strconv"
)

func compactJSON(tVal []byte) []byte {
	buffer := new(bytes.Buffer)
	json.Compact(buffer, tVal)
	return buffer.Bytes()
}

func isJSON(s string) bool {
	var js map[string]interface{}
	return json.Unmarshal([]byte(s), &js) == nil
}

func GetJSON(jsonStream interface{}) (interface{}, error) {
	data, err := json.Marshal(jsonStream)
	if err != nil {
		return "", err
	}
	var tempData interface{}
	err = json.Unmarshal(data, &tempData)
	if err != nil {
		return "", err
	}

	switch t := tempData.(type) {
	case float64:
		tVal := strconv.FormatFloat(tempData.(float64), 'f', 2, 64)
		return compactJSON([]byte(tVal)), nil
	case string:
		val := ""
		if isJSON(tempData.(string)) {
			val = string(compactJSON([]byte(tempData.(string))))
		} else {
			val = string(tempData.(string))
		}
		return val, nil
	case map[string]interface{}:
		return compactJSON(data), nil
	case []interface{}:
		return compactJSON(data), nil
	default:
		fmt.Println("type:", t)
	}
	return "", nil
}
