package conversion

import (
	"bytes"
	"encoding/gob"
)

var registered = false

func registerTypes() {
	gob.Register(map[string]interface{}{})
	registered = true
}

// GetBytes return the bytes of any interface
func GetBytes(key interface{}) ([]byte, error) {
	if !registered {
		registerTypes()
	}
	var buf bytes.Buffer
	enc := gob.NewEncoder(&buf)
	err := enc.Encode(key)
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}
