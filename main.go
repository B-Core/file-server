package main

import (
	"adbuilder-server/routes"
	"adbuilder-server/server"
)

func main() {
	router := server.CreateServer()
	routes.AttachRoute("projects", router)
	router = server.StartServer(router)
}