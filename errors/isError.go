package errors

import "fmt"

// IsError check if the object is an error
// and returns a booelan
// also print the error on stdout
func IsError(err error) bool {
	if err != nil {
		fmt.Println(err.Error())
	}

	return (err != nil)
}
