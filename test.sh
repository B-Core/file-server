#! bin/bash

rm -rf database/testDB/
rm -rf coverage/
mkdir coverage
for f in ./*
do
  if [[ -d $f ]]; then
      # $f is a directory
      package=${f#*./}
      ENV="test" go test adbuilder-server/$package -cover -coverprofile=coverage/$package.out
      go tool cover -html=coverage/$package.out -o coverage/$package.html
  fi
done